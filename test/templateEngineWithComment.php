<?php
// création de la class TemplateEngine
class TemplateEngine
{
    /**
     * @var string
     */
    // création et privatisation de $templateDir
    private $templateDir;

    // création de la méthode
    public function __construct($templateDir)
    {
        // déclare que $templateDir appartient à la 
        // public function __construct($templateDir)
        $this->templateDir = $templateDir;
    }

    /**
     * {@inheritDoc}
     */
    public function render($template, array $parameters = array())
    {
        extract($parameters);

        if (false === $this->isAbsolutePath($template)) {
            // DIRECTORY_SEPARATOR : / or \ (Adaptation au system)
            $template = $this->templateDir . DIRECTORY_SEPARATOR . $template;
        }
        // ob_start : Enclenche la temporisation de sortie
        ob_start();
        include $template;
        // ob_get_clean : Lit le contenu courant du tampon de sortie puis l'efface
        return ob_get_clean();
    }
    // création et privatisation de la méthode isAbsolutePath()
    private function isAbsolutePath($file)
    {
        // strspn :  Trouve la longueur du segment initial d'une chaîne contenant tous les caractères d'un masque donné
        // strlen : Calcule la taille d'une chaîne
        // ctype_alpha : Vérifie qu'une chaîne est alphabétique
        // substr : Retourne un segment de chaîne
        if (strspn($file, '/\\', 0, 1)
            || (strlen($file) > 3 && ctype_alpha($file[0])
            && substr($file, 1, 1) === ':'
            && (strspn($file, '/\\', 2, 1))
        )
        // parse_url : Analyse une URL et retourne ses composants
        || null !== parse_url($file, PHP_URL_SCHEME)
        ) {
            return true;
        }

        return false;
    }
}