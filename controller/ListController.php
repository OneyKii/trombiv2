<?php
/* Data de base donner par Kristen
http://app-d6e4214a-9a28-46b5-967f-565f071952c1.cleverapps.io/students */

/* Data venant du PC de Thomas peresson
192.168.1.14/simplonP2/apiwp/wp-json/apiedn/v1/apprenants */

class ListController
{
    CONST APIURL = 'https://www.lecoledunumerique.fr/wp-json/wp/v2/apprenants';
    private $templateEngine;
    private $model;
    

    function __construct($templateEngine, $model) {
        $this->templateEngine = $templateEngine;
        $this->model = $model;
    }

    public function diplayList() {
        $studentData = $this->model->getData(self::APIURL);

        return $this->templateEngine->render('studentData.php', ['studentData' => $studentData]);
    }

}

