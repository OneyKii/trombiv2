<div class="card-container">

            <div class="data">
                <img class="img-img" src="https://picsum.photos/200"/>
            </div> 

            <div class="data">
            <?php
                foreach ($value['title'] as $title) {
                    printf('<li>' . $title['rendered'] . '</li>');
                } 
            ?>        
            </div>


            <button class="course-accordion">En savoir +</button>
			<div class="course-panel">
				<div class="div-categoryContainer">
					<div class="paragraph-right">

                        <div class="data paragraph-extrait"><?php printf($value['extrait']) ?></div>
					</div>
				</div>
			</div>

            <div class="data"></div>

            <ul class="data competence">
                <?php
                foreach ($value['competences'] as $competence) {
                    printf('<li>' . $competence['name'] . '</li>');
                }
                ?>
            </ul>

            <div class="data">
                <?php 
                $promotion = $value['promotion'][0];
                printf($promotion['description']);
                ?>
            </div>

  
            <div class="data"><a href="<?php printf($value['cv']); ?>" target="_blank">
                    <img src="https://img.icons8.com/material-outlined/24/000000/download--v1.png" />
                </a>
            </div>


            <div class="data">
                <a href="<?php printf($value['linkedin']); ?>" target="_blank">
                    <img class="linkedin" src="https://img.icons8.com/color/48/000000/linkedin.png"/>
                </a>
            </div>

        </div>