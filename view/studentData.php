<?php
include 'view/header.php';
?>

<div class="classification">

    <div class="select">
        <!-- Selection du Type de développeur -->
        <select value="type" placeholder="Type">
            <option value="BackEnd">Back End</option>
            <option value="FrontEnd">Front End</option>
        </select>

        <!-- Selection de la promotion souhaité -->
        <select name="promo" id="promo">
                <!-- filtre à faire en PhP -->
            <option value="promotion-<?php // numéro de la promo ?>">Promotion 1</option>
            <option value="promotion-<?php // numéro de la promo ?>"> Promotion 2</option>
        </select>
    </div>


    <div class="search-bar">
        <input type="search" name="search" id="search">
        <input type="button" value="rechercher">
    </div>
    
</div>
    <div class="container">
        <?php

        foreach ($studentData as $values) { ?>

        <?php
            echo '<div class="card-container">';

                // Photo de profil
            printf('%s',  '<img class="img-responsive" src="' . $values['image'] . '">');

                // Nom & Prénom
            printf('%s', '<h4 class="name">' . $values['title']['rendered'] . '</h4>');

                // Promotion
            printf('%s', '<p class="promotion">' . $values['promotion']['name'] . '</p>');

                // type
            echo '<div class="type">';
            foreach ($values['competences'] as $type) {
                printf('%s', '<p>' . $type['name'] . '</p>');
            }
            echo '</div>';

            // extrait
            printf('%s', '<div class="extrait">' . $values['excerpt']['rendered'] . '</div>');


            echo '<div class="social-network-global">';

                        // Portfolio 
                    $linkImgPortfolio = 'https://img.icons8.com/material-rounded/48/ffffff/user.png';
                    $linkPortfolio = $values['portfolio'];
                printf('%s', '<a href="' . $linkPortfolio . '" target="_blank"><img class="social-network" src="' . $linkImgPortfolio . '"></a>');

                        // linkedin
                    $linkImgLinkedin = 'https://img.icons8.com/material-outlined/48/ffffff/linkedin.png';
                    $linkLinkedin = $values['linkedin'];
                printf('%s', '<a href="' . $linkLinkedin . '" target="_blank"><img class="social-network" src="' . $linkImgLinkedin . '"></a>');

                        // CV 
                    $linkImgCV = 'https://img.icons8.com/material-outlined/48/ffffff/download--v1.png';
                    $linkCV = $values['cv'];
                printf('%s', '<a href="' . $linkCV . '" target="_blank"><img class="social-network" src="' . $linkImgCV . '"></a>');

                // End .social-network-global
            echo '</div>';
            // End .card-container
            echo '</div>';








            // End foreach
        }

        ?>
        <!-- End .container -->
    </div>

<?php

include 'view/footer.php';
?>