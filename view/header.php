<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="asset/css/style.css">
    <link rel="stylesheet" href="asset/css/header.css">
    <link rel="stylesheet" href="asset/css/footer.css">
    <link rel="stylesheet" href="asset/css/responsive.css">
    <title>Document</title>
</head>

<body>

    <header class="page-header">
        <img class="page-header" src="asset/img/navbar-header.png" alt="" srcset="">
    </header>

    <div class="container-header">
        <img src="asset/img/groupe.jpg" alt="" class="background-top">
        <div class="text-container-header">
            <h1>Trombinoscope</h1>
            <p>Lorem ipsum dolor</p>
        </div>
    </div>
