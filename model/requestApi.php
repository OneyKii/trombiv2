<?php

class GetApiData
{

    public function getData($url)
    {
        $curl = curl_init($url);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($curl);
        $data = json_decode($data, true);

        return $data;
    }
}

// initialisation de la class GetApiData
/*$getApiData = new GetApiData();
$studentData = $getApiData->getData($linkGetData);*/